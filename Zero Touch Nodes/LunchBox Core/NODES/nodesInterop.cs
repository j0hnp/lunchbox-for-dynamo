﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Xml;

using ProvingGround.ExcelInterop;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

namespace Interoperability
{
    /// <summary>
    /// LunchBox Excel utilities.
    /// </summary>
    public static class Excel
    {
        /// <summary>
        /// Get Excel File Info
        /// </summary>
        /// <param name="File">Excel File</param>
        /// <returns name="WorkbookName">Nameo of the Excel workbook.</returns>
        /// <returns name="WorksheetNames">Names of the Excel worksheets.</returns>
        /// <search>lunchbox,lists,rows,excel,dataset,datatable</search>
        [MultiReturn(new[] { "WorkbookName", "WorksheetNames" })]
        public static Dictionary<string, object> GetExcelFileInfo(object File)
        {

            string m_wbName = null;
            List<string> m_wsNames = null;

            if (File is FileInfo)
            {
                FileInfo filenfo = (FileInfo)File;
                string path = filenfo.FullName;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_wbName = pg.WorkbookName;
                m_wsNames = pg.WorksheetNames;
                pg.Close();
            }
            else if (File is string)
            {
                string path = (string)File;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_wbName = pg.WorkbookName;
                m_wsNames = pg.WorksheetNames;
                pg.Close();
            }

            return new Dictionary<string, object>
      {
        {"WorkbookName", m_wbName},
        {"WorksheetNames", m_wsNames}
      };
        }

        /// <summary>
        /// Get All Excel Worksheet Data
        /// </summary>
        /// <param name="File">Excel File</param>
        /// <param name="IsVisible">Boolean to determine if Workbook is visible.</param>
        /// <param name="UseHeaders">Boolean to determine if the first row used for headers.</param>
        /// <param name="WorksheetName">Name of the worksheet.</param>
        /// <returns name="DataTable">Worksheet as DataTable.</returns>
        /// <search>lunchbox,lists,rows,excel,dataset,datatable</search>
        [MultiReturn(new[] { "DataTable" })]
        public static Dictionary<string, object> GetExcelWorksheetData(object File, bool IsVisible, bool UseHeaders, string WorksheetName)
        {
            DataTable m_dt = null;

            if (File is FileInfo)
            {
                FileInfo filenfo = (FileInfo)File;
                string path = filenfo.FullName;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_dt = pg.GetAllDataFromWorksheet(WorksheetName, UseHeaders);
                pg.Close();
            }
            else if (File is string)
            {
                string path = (string)File;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_dt = pg.GetAllDataFromWorksheet(WorksheetName, UseHeaders);
                pg.Close();
            }

            return new Dictionary<string, object>
      {
        {"DataTable", m_dt}
      };
        }

        /// <summary>
        /// Get Excel Worksheet Data by Range
        /// </summary>
        /// <param name="File">Excel File</param>
        /// <param name="IsVisible">Is Excel visible.</param>
        /// <param name="UseHeaders">Read first row as headers.</param>
        /// <param name="WorksheetName">Name of the worksheet.</param>
        /// <param name="RowStart">The starting row index.</param>
        /// <param name="RowEnd">The ending row index.</param>
        /// <param name="ColStart">The starting column index.</param>
        /// <param name="ColEnd">The ending column index.</param>
        /// <returns name="DataTable">Worksheet as DataTable.</returns>
        /// <search>lunchbox,lists,rows,excel,dataset,datatable</search>
        [MultiReturn(new[] { "DataTable" })]
        public static Dictionary<string, object> GetExcelWorksheetDataByRange(object File, bool IsVisible, bool UseHeaders, string WorksheetName, int RowStart, int ColStart, int RowEnd, int ColEnd)
        {
            DataTable m_dt = null;


            if (File is FileInfo)
            {
                FileInfo filenfo = (FileInfo)File;
                string path = filenfo.FullName;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_dt = pg.GetDataByRangeFromWorksheet(WorksheetName, RowStart, ColStart, RowEnd, ColEnd, UseHeaders);
                pg.Close();
            }
            else if (File is string)
            {
                string path = (string)File;

                ProvingGround.ExcelInterop.ExcelReadEntry pg = new ProvingGround.ExcelInterop.ExcelReadEntry(path, false);
                pg.Open();
                m_dt = pg.GetDataByRangeFromWorksheet(WorksheetName, RowStart, ColStart, RowEnd, ColEnd, UseHeaders);
                pg.Close();
            }

            return new Dictionary<string, object>
      {
        {"DataTable", m_dt}
      };
        }
    }
}
