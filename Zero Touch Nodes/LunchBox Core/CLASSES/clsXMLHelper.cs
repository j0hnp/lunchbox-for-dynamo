﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LunchBoxClasses
{
  class clsXMLHelper
  {
    private XmlNode _xml;

    public List<object> Nodes;
    public List<object> Values;

    public clsXMLHelper(XmlNode xmldoc)
    {
      //widen scope

      _xml = xmldoc;

      Nodes = new List<object>();
      Values = new List<object>();

      Nodes = GetXMLNodes(Nodes, _xml);
      Values = GetXMLValues(Values, _xml);
    }

    /// <summary>
    /// Get XML Nodes
    /// </summary>
    /// <param name="parentbranch"></param>
    /// <param name="xml"></param>
    /// <returns></returns>
    public List<object> GetXMLNodes(List<object> parentbranch, XmlNode xml)
    {
      foreach (XmlNode m_childnode in xml.ChildNodes)
      {
        List<object> m_treebranch = new List<object>();

        if (m_childnode.HasChildNodes == true)
        {
          m_treebranch.Add(m_childnode.Name);
          m_treebranch = GetXMLNodes(m_treebranch, m_childnode);
        }

        if (m_treebranch.Count > 0 )
        {
          parentbranch.Add(m_treebranch);
        }
      }

      return parentbranch;
    }

    /// <summary>
    /// Get XML Values
    /// </summary>
    /// <param name="parentbranch"></param>
    /// <param name="xml"></param>
    /// <returns></returns>
    public List<object> GetXMLValues(List<object> parentbranch, XmlNode xml)
    {
      foreach (XmlNode m_childnode in xml.ChildNodes)
      {
        List<object> m_treebranch = new List<object>();

        if (m_childnode.HasChildNodes == true)
        {
          m_treebranch = GetXMLValues(m_treebranch, m_childnode);
        }
        else
        {
          m_treebranch.Add(m_childnode.Value);
        }

        if (m_treebranch.Count > 0)
        {
          parentbranch.Add(m_treebranch);
        }
      }

      return parentbranch;
    }

  }
}
