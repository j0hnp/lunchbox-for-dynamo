﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.Tesselation.CLASSES
{
    /// <summary>
    /// Line class
    /// </summary>
    public class clsLine
    {
        #region Private Mebers
        private clsPoint _a;
        private clsPoint _b;
        #endregion

        #region Public Properties
        /// <summary>
        /// Corner point A
        /// </summary>
        public clsPoint A
        {
            get
            {
                return _a;
            }
            set
            {
                _a = A;
            }
        }

        /// <summary>
        /// Corner point B
        /// </summary>
        public clsPoint B
        {
            get
            {
                return _b;
            }
            set
            {
                _b = B;
            }
        }
        #endregion

        /// <summary>
        /// Create new Quad
        /// </summary>
        /// <param name="a">Corner A</param>
        /// <param name="b">Corner B</param>
        public clsLine(clsPoint a, clsPoint b)
        {
            _a = a;
            _b = b;
        }
    }
}
