# LunchBox for Dynamo #

This is a repository for the LunchBox for Dynamo project. This collection of tools exposes capabilities for paneling, geometry management, and Revit automation.
This repository is not under active development.

### How do I get set up? ###

* Download the repository
* Open the Visual Studio solution file
* Confirm that the project is pointed to correct Revit and Dynamo APIs
* Build the solution

### Contribution guidelines ###

*  Fork the LunchBox repository
*  Make cool stuff.
*  Submit a Pull request.

### GNU General Public License ###

LunchBox is an open source project under the [GNU Lesser General Public](https://www.gnu.org/licenses/lgpl-3.0.en.html) license

Copyright (c) 2020 [Proving Ground LLC](http://ProvingGround.io)

This library is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or (at 
your option) any later version.

The copyright holders provide no reassurances that the source code provided
does not infringe any patent, copyright, or any other intellectual property
rights of third parties. The copyright holders disclaim any liability to 
any recipient for claims brought against recipient by any third party for 
infringement of that parties intellectual property rights.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
for more details.